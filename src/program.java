import java.util.Scanner;

public class program 
{

	public static void main(String[] args) 
	{
		//part 1
		String name="Kiran";
		System.out.println(studying(name));
		//part 2
		name="";
		System.out.println(studying(name));
		//part3
		name="ALEN";
		System.out.println(studying(name));
		//part3
		name="dave,John";
		System.out.println(studying(name));
		
	}
	public static String studying(String name)
	{
		
		if(name=="")
			return "Nobody is studying";
		else if(upper(name))
			return name+" IS STUDYING";
		else if(two(name))
		{
			int len=0;
			for(int i=0;i<name.length();i++)
			{	
				if(name.charAt(i)==',')
					len =i;
			}
			return name.substring(0,len) +" and "+  name.substring(len+1) +" are studying ";
		}
		else
			return name+" is studying";
	}
	public static boolean upper(String name)
	{
		for(int i=0;i<name.length();i++)
		{
			int n=name.charAt(i);
			if(n<65 || n>90)
				return false;
				
		}
		return true;
	}
	public static boolean two(String name)
	{
		for(int i=0;i<name.length();i++)
		{
			if(name.charAt(i)==',')
				return true;
		}
		return false;
	}
}
